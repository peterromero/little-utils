<?php

require_once 'src/LittleUtils/Factorizer.php';

use PHPUnit\Framework\TestCase;
use LittleUtils\Factorizer;

final class FactorizerTest extends TestCase {
  public function testCantInstantiateWithOne() {
    $this->expectExceptionCode(\LittleUtils\FACTORIZER_ERROR_INPUT_TOO_SMALL);
    $x = new Factorizer(1);
  }

  public function testCantInstantiateWithZero() {
    $this->expectExceptionCode(\LittleUtils\FACTORIZER_ERROR_INPUT_TOO_SMALL);
    $x = new Factorizer(0);
  }

  public function testCantInstantiateWithNegative() {
    $this->expectExceptionCode(\LittleUtils\FACTORIZER_ERROR_INPUT_TOO_SMALL);
    $x = new Factorizer(-1);
  }

  public function testCantInstantiateWith64Bit() {
    $this->expectExceptionCode(\LittleUtils\FACTORIZER_ERROR_INPUT_TOO_LARGE);
    $x = new Factorizer(10000000000);
  }

  public function testCantInstantiateWithFraction() {
    $this->expectExceptionCode(\LittleUtils\FACTORIZER_ERROR_INPUT_NOT_INTEGER);
    $x = new Factorizer(5.6);
  }

  public function testFactorArray37() {
    $x = new Factorizer(37);
    $f = $x->get_prime_factors_array();
    $this->assertInternalType('array', $f);
    $this->assertEquals([37], $f);
  }

  public function testFactorArray60() {
    $x = new Factorizer(60);
    $f = $x->get_prime_factors_array();
    $this->assertInternalType('array', $f);
    $this->assertEquals([2, 2, 3, 5], $f);
  }

  public function testFactorArray100() {
    $x = new Factorizer(100);
    $f = $x->get_prime_factors_array();
    $this->assertInternalType('array', $f);
    $this->assertEquals([2, 2, 5, 5], $f);
  }

  public function testFactorArray51616944() {
    $x = new Factorizer(51616944);
    $f = $x->get_prime_factors_array();
    $this->assertInternalType('array', $f);
    $this->assertEquals([2, 2, 2, 2, 3, 3, 347, 1033], $f);
  }

  public function testFactorString48() {
    $x = new Factorizer(48);
    $f = $x->get_prime_factors_string();
    $this->assertInternalType('string', $f);
    $this->assertEquals('2<sup>4</sup> × 3', $f);
  }

  public function testFactorString61() {
    $x = new Factorizer(61);
    $f = $x->get_prime_factors_string();
    $this->assertInternalType('string', $f);
    $this->assertEquals('61', $f);
  }
}
