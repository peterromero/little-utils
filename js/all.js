$(function () {
  // Prevent clicks on .nolink a's (mostly icons) from going anywhere
  $('a.nolink').click(function (e) {
    e.preventDefault();
  });

  // Open and close the modal popup with the tool description.
  $('a.info').click(function () {
    $('#modal-shade-info').addClass('open');
  });
  $('div.info .close, #modal-shade-info').click(function () {
    $('#modal-shade-info').removeClass('open');
  });

  // Open and close the hamburger menu.
  $('a.hamburger-opener').click(function () {
    $(this).toggleClass('open');
    $('#toolbox').toggleClass('open');
  });

  // Select text field contents when fields are focused.
  $('input[type=date], input[type=datetime-local], input[type=email], input[type=month], input[type=number], input[type=password], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week]').focus(function () {
    $(this).select();
  }).keydown(function (e) {
    if (e.which == 13) {
      $(this).select();
    }
  });

  // Intercept form submissions and change them to AJAX.
  $('form').submit(function (e) {
    e.preventDefault();

    var ajaxPath = $(this).attr('action');

    $.ajax({
      type: "POST",
      url: ajaxPath,
      data: $(this).serialize(),
      dataType: 'json'
    }).done(function (data) {
      handleAjaxReturn(data);
    });
  });
});
