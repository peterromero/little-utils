<?php

require_once '../core/config.php';

$integer = new \LittleUtils\Factorizer($_POST['integer']);

$return = [
  'result' => $integer->get_prime_factors_string()
];

print json_encode($return);
