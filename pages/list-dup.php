<div class="field">
  <label for="list_items">List Items (one per line)</label>
  <textarea name="list_items" id="list_items" rows="5"></textarea>
</div>
<div class="field">
  <input type="checkbox" name="sort_output" id="sort_output">
  <label for="sort_output" class="inline">Sort Output</label>
</div>
<div class="field">
  <input type="checkbox" name="case_sensitive" id="case_sensitive">
  <label for="case_sensitive" class="inline">Case-Sensitive</label>
</div>
<div class="field">
  <input type="submit" value="Eliminate Duplicates">
</div>
<div class="field">
  <label for="result">Result <a class="copy nolink" href="#"></a></label>
  <textarea name="result" id="result" rows="5" disabled="disabled"></textarea>
</div>
