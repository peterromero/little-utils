<div class="field">
  <label for="integer">Integer to Factor</label>
  <input type="number" name="integer" id="integer" value="60" min="2" step="1">
</div>
<div class="field">
  <input type="submit" value="Find Factors">
</div>
<div class="field">
  <label>Prime Factorization <a class="copy nolink" href="#"></a></label>
  <div id="result">2<sup>2</sup> × 3 × 5</div>
</div>
