<?php

function render_page($path) {
  global $registered_paths;

  // Get absolute path to directory containing theme.php
  $dir = str_replace('\\', '/', realpath(dirname(__FILE__)));

  if (empty($path)) {
    $path = 'home';
  }

  // Generate the path to the page's content file.
  $content_file_path = $dir . "/../pages/$path.php";

  // If file doesn't exist, redirect to 404 page.
  if (!file_exists($content_file_path)) {
    $content_file_path = $dir . "/../pages/404.php";
    $path = '404';
  }

  // Generate the path to the page's specific Javascript.
  $script_file_path_absolute = $dir . "/../js/$path.js";
  if (file_exists($script_file_path_absolute)) {
    $script_file_path = "js/$path.js";
  }
  else {
    $script_file_path = NULL;
  }

  // Generate the path to the page's specific CSS.
  $style_file_path_absolute = $dir . "/../stylesheets/css/$path.css";
  if (file_exists($style_file_path_absolute)) {
    $style_file_path = "../stylesheets/css/$path.css";
  }
  else {
    $style_file_path = NULL;
  }

  // Generate the path to the page's AJAX form processor.
  $ajax_file_path_absolute = $dir . "/../ajax/$path.php";
  if (file_exists($ajax_file_path_absolute)) {
    $ajax_file_path = "/ajax/$path.php";
  }
  else {
    $ajax_file_path = NULL;
  }

  $toolbox_contents = get_toolbox_contents();

  // Include the HTML wrapper, which in turn includes the page content.
  require_once $dir . "/../templates/html.php";
}

function get_toolbox_contents() {
  global $registered_paths;

  $categories = [];
  foreach ($registered_paths as $path => $info) {
    if ($path != 'home' && $path != '404') {
      if (empty($categories[$info['category']])) {
        $categories[$info['category']] = [];
      }
      $categories[$info['category']][] = [
        'title' => $info['h1'],
        'path' => $path
      ];
    }
  }

  ksort($categories);

  $output = [];
  foreach ($categories as $category_title => $items) {
    usort($items, function ($a, $b) {
      if ($a['title'] < $b['title']) {
        return -1;
      }
      return 1;
    });
    $output[] = "<h2>$category_title</h2>";
    $output[] = '<ul>';
    foreach ($items as $item) {
      $output[] = "<li><a href='/{$item['path']}'>{$item['title']}</a></li>";
    }
    $output[] = '</ul>';
  }
  return implode('', $output);
}
