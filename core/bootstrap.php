<?php

require_once 'core/config.php';

// Grab the path from the server request URI.
$request_path = strtok($_SERVER['REQUEST_URI'], '?');
$base_path_len = strlen(rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/'));
$path = substr(urldecode($request_path), $base_path_len + 1);
if ($path == basename($_SERVER['PHP_SELF'])) {
  $path = '';
}
$path = trim($path, '/');
