<?php

// Set up class autoloading.
spl_autoload_register('little_utils_autoloader');
function little_utils_autoloader($class_name) {
  global $base_filepath;
  require_once $base_filepath . "/src/$class_name.php";
}
