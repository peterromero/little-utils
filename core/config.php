<?php

$base_uri = 'http://harmony.localhost';
$base_filepath = 'C:/xampp/htdocs/harmony';

require_once $base_filepath . '/core/autoloader.php';

// REGISTERED PATHS
// Each array key is a path. The options it may have are:
// - title: The page title (in the <title> tag)
// - h1: The page title (in the <h1> tag)
// - info: The description of the page's function for the ? icon
// - category: The section this tool will show up in in the toolbox.
$registered_paths = [
  'home' => [
    'title' => 'Home',
  ],
  '404' => [
    'title' => 'Page Not Found',
    'h1' => '404'
  ],
  'harmony' => [
    'title' => 'Harmonic Analyzer',
    'h1' => 'Harmonic Analyzer',
    'category' => 'Music'
  ],
  'list-dup' => [
    'title' => 'List Duplicate Eliminator',
    'h1' => 'List Duplicate Eliminator',
    'category' => 'Text &amp; Lists',
    'info' => 'This tool eliminates duplicate items from a list. Enter a list of items, each on its own line, and click "Eliminate Duplicates". You can copy the result to the clipboard by clicking the icon next to "Result."'
  ],
  'list-comp' => [
    'title' => 'List Comparer',
    'h1' => 'List Comparer',
    'category' => 'Text &amp; Lists',
    'info' => 'Write something here'
  ],
  'text-sanitize' => [
    'title' => 'Text Sanitizer',
    'h1' => 'Text Sanitizer',
    'category' => 'Text &amp; Lists',
    'info' => 'Write something here'
  ],
  'frac-reduce' => [
    'title' => 'Fraction Reducer',
    'h1' => 'Fraction Reducer',
    'category' => 'Math',
    'info' => 'Write something here'
  ],
  'gcd' => [
    'title' => 'GCD Calculator',
    'h1' => 'GCD Calculator',
    'category' => 'Math',
    'info' => 'Write something here'
  ],
  'lcm' => [
    'title' => 'LCM Calculator',
    'h1' => 'LCM Calculator',
    'category' => 'Math',
    'info' => 'Write something here'
  ],
  'all-factors' => [
    'title' => 'Integer Factorizer',
    'h1' => 'Integer Factorizer (All Factors)',
    'category' => 'Math',
    'info' => 'Write something here'
  ],
  'prime-factors' => [
    'title' => 'Prime Factorizer',
    'h1' => 'Integer Factorizer (Prime Factors)',
    'category' => 'Math',
    'info' => 'Write something here'
  ],
  'dec2frac' => [
    'title' => 'Fraction from Decimal',
    'h1' => 'Fraction from Decimal',
    'category' => 'Math',
    'info' => 'Write something here'
  ],
];
