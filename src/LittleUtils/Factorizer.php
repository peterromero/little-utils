<?php

namespace LittleUtils;

const FACTORIZER_ERROR_INPUT_TOO_SMALL = 1;
const FACTORIZER_ERROR_INPUT_TOO_LARGE = 2;
const FACTORIZER_ERROR_INPUT_NOT_INTEGER = 3;

class Factorizer {
  private $integer;
  private $factors = NULL;

  public function __construct($integer) {
    if ($integer < 2) {
      throw new \Exception('Cannot factor integers less than 2.', FACTORIZER_ERROR_INPUT_TOO_SMALL);
    }
    if ($integer > 4294967296) {
      throw new \Exception('Cannot factor integers greater than 4294967296.', FACTORIZER_ERROR_INPUT_TOO_LARGE);
    }
    if ($integer != (int) $integer) {
      throw new \Exception('Cannot factor non-integers.', FACTORIZER_ERROR_INPUT_NOT_INTEGER);
    }
    $this->integer = $integer;
  }

  private function calc_prime_factors() {
    $int_copy = $this->integer;
    $upper_search_bound = ceil(sqrt($int_copy));
    $factors = [];
    for ($i = 2; $i <= $upper_search_bound; $i++) {
      if ($int_copy / $i === (int) ($int_copy / $i)) {
        $factors[] = $i;
        $int_copy /= $i;
        $upper_search_bound = ceil(sqrt($int_copy));
        $i--;
      }
    }
    if ($int_copy > 1) {
      $factors[] = $int_copy;
    }
    $this->factors = $factors;
  }

  public function get_prime_factors_array() {
    if (!$this->factors) {
      $this->calc_prime_factors();
    }
    return $this->factors;
  }

  public function get_prime_factors_string() {
    if (!$this->factors) {
      $this->calc_prime_factors();
    }

    $occurrences = [];
    foreach ($this->factors as $factor) {
      if (empty($occurrences[$factor])) {
        $occurrences[$factor] = 1;
      }
      else {
        $occurrences[$factor]++;
      }
    }

    $factors = [];
    foreach ($occurrences as $base => $exponent) {
      if ($exponent == 1) {
        $factors[] = $base;
      }
      else {
        $factors[] = "$base<sup>$exponent</sup>";
      }
    }

    return implode(' × ', $factors);
  }
}
