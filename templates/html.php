<html>
  <head>
    <title>Little Utils
      | <?php print $registered_paths[$path]['title'] ?></title>
    <link href="https://fonts.googleapis.com/css?family=Arimo:700|Playfair+Display:400,700" rel="stylesheet">
    <link rel="stylesheet" href="../stylesheets/css/master.css">
    <link rel="stylesheet" href="../stylesheets/css/master.css">
    <?php
    if ($style_file_path) {
      print "<link rel='stylesheet' href='$style_file_path'>";
    }
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/all.js"></script>
    <?php
    if ($script_file_path) {
      print "<script src='$script_file_path'></script>";
    }
    ?>
  </head>
  <body>
    <div id="header">
      <a href="/" class="logo"></a>
      <a href="#" class="hamburger-opener nolink"></a>
    </div>
    <div id="toolbox">
      <?php
      print $toolbox_contents;
      ?>
    </div>
    <div id="content">
      <?php
      if (!empty($registered_paths[$path]['h1'])) {
        print "<h1>";
        print $registered_paths[$path]['h1'];
        if (!empty($registered_paths[$path]['info'])) {
          print "<a class='info nolink'></a>";
        }
        print "</h1>";
      }
      ?>
      <form action="<?php if ($ajax_file_path) {
        print $ajax_file_path;
      } ?>">
        <?php require_once $content_file_path; ?>
      </form>
    </div>
    <?php
    if (!empty($registered_paths[$path]['info'])) {
      print "<div id='modal-shade-info'><div class='info'><a href='#' class='close nolink'></a><p>{$registered_paths[$path]['info']}</p></div></div>";
    }
    ?>
  </body>
</html>
